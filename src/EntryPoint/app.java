package EntryPoint;



/**
 * Internal imports
 * 
 * */
import Communication.Server;
import ThreadManager.ThreadManager_generic;
import ErrorManager.ErrorManager_generic;




/**
 * Java imports
 * 
 * */




/**
 * 
 * Purpose of class:
 * 				-entry point for application
 * 
 * */






public class app {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		ErrorManager_generic err=new ErrorManager_generic();
		ThreadManager_generic tmng=new ThreadManager_generic();
		
		tmng.thread_Init(new Server(),"open",err,tmng,ThreadManager_generic.class,0);
	}

}
