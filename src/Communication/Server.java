package Communication;



/**
 * 
 * Internal imports
 * 
 * */
import ErrorManager.ErrorManager_generic;
import ThreadManager.ThreadManager_flagCarrier;
import ThreadManager.ThreadManager_generic;
import Communication.ClientApi;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;

/**
 * 
 * Java imports
 * 
 * */

import java.io.IOException;
import java.net.Inet4Address;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Random;

import com.sun.xml.internal.ws.api.pipe.ThrowableContainerPropertySet;




/**
 * Created by LeNo 6.9.2017..
 * 
 * 
 * Purpose of class:
 * 			-"listener" class.
 * 			  It just listen for connection,when connection is established it will create new "ClientApi" object and pass the input and 
 * 			  output stream.
 * 
 * */

public class Server {
	
	private final String passwordEntry="c";
	
	private ServerSocket server;
	private ErrorManager_generic errMng;
	
	private ArrayList<ClientApi> clConnected;
	
	public void open(ErrorManager_generic err,ThreadManager_generic tMng) {
		
		try 
		{
			clConnected=new ArrayList<>();
			server=new ServerSocket(3000);
			errMng=err;
			acceptConnections(tMng);
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}
	
	private void acceptConnections(ThreadManager_generic tMng) {
		
		
		try 
		{
			System.out.println("Server started");
			while(true)
			{
				Socket client=server.accept();
				System.out.println("Client connected "+client.getInetAddress().getHostAddress());
				checkAddress(client, tMng);
			}
			
		}
		catch(IOException e)
		{
			errMng.handler(e);
		}
	}
	
	
	
	private void checkAddress(Socket client,ThreadManager_generic tMng)
	{
		try 
		{
		
			byte[] bytes=new byte[2];
			
			BufferedInputStream inStream=new BufferedInputStream(client.getInputStream());
			BufferedOutputStream outStream=new BufferedOutputStream(client.getOutputStream());
			
			StringBuilder builder=new StringBuilder();
			
			while(inStream.read(bytes)!=-1)
			{
				
				
				if (bytes[0]==63 && bytes[1]==92)
				{
					outStream.write((Integer.toString(6392)+System.getProperty("user.name")).getBytes());
					outStream.flush();
				}
				
				
				else if (concatPassword(builder, bytes)) 
				{
					if (builder.toString().equals(passwordEntry)) 
					{
						createClientApi(outStream, inStream, client, tMng);;
					}
					else 
					{
						outStream.write((byte)0);
						outStream.flush();
					}
				}			
			}
			System.out.println("Nema podataka");
		}
		catch(IOException e) 
		{
			System.out.println("Connection is not valid");
		}
	}
	
	private boolean concatPassword(StringBuilder pass,byte[] bytes) {
		
		for (byte b: bytes) 
		{
			if ((char)b=='*')return true;
			
			else pass.append((char)b);
			
		}
		return false;
	}
	
	
	/**
	 * 
	 * client object functions
	 * 
	 * */
	
	private void createClientApi(
								 BufferedOutputStream outStream,
								 BufferedInputStream inStream,
								 Socket client,
								 ThreadManager_generic tMng) throws IOException 
	{
	
		String session=generateSession();
		
		ClientApi clientObject=new ClientApi(client.getInetAddress().getHostAddress(),session,inStream, outStream, tMng, errMng);
		
		clConnected.add(clientObject);
		
		outStream.write(session.getBytes());
		outStream.flush();
		
		client.setSoTimeout(10);
		clientObject.requestListen();
	}
	
	
	private String generateSession() 
	{
		StringBuilder session=new StringBuilder();
		Random rand=new Random();
		
		for (int i=0; i<50; i++) 
		{
			session.append(Integer.toString(rand.nextInt(9)));
		}
		
		return session.append("*").toString();
	}
}
