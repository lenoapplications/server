package Communication;


/**
 * Internal imports
 * */
import ThreadManager.ThreadManager_flagCarrier;


/**
 * Java imports
 * 
 * */


/**
 * Created by LeNo 15.9.2017..
 * 
 * 
 * Purpose of class:
 * 			-class for handling received bytes from input stream.
 * 
 * 			Description:
 * 					recvRequest function-> checks if there is some data available in stream,if there is some data, it will add those bytes
 * 					to "builder",which turns the bytes into string. If string contains '*',that means that the string is complete and can be
 * 					procced to next phase ("identRequest function in "ClientApi")
 * 
 * */

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.Arrays;


public class ByteManager {
	
	
	private byte[] bytes;
	private final StringBuilder builder;
	private final ThreadManager_flagCarrier flgCarr;
	
	public ByteManager(ThreadManager_flagCarrier flgC) 
	{
		bytes=new byte[1024];
		builder=new StringBuilder();
		flgCarr=flgC;
	}
	
	
	
	public boolean recvRequest(BufferedInputStream inStream,BufferedOutputStream outStream) throws IOException
	{
		int bytesRead=0;
		try 
		{
			bytesRead=inStream.read(bytes);
						
			builder.append(new String(bytes,0,bytesRead));
					
			return builder.charAt(builder.length()-1)=='*';
			
		}catch(SocketTimeoutException  | StringIndexOutOfBoundsException s) 
		{
			if (bytesRead==-1)throw new IOException("Error:connection break down");
			
			outStream.flush();
			
			return false;
		}
		
	}
	
	
	/**
	 * 
	 * STRINGBUILDER
	 * 
	 * */
	
	public String getRequest() 
	{
		return builder.toString();
	}
	
	
	/**
	 * 
	 * CLEAN after request
	 * 
	 * */
	
	public void cleanRequest() 
	{
		Arrays.fill(bytes, (byte)0);
		builder.setLength(0);
	}
}
