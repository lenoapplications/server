package Communication;

/**
 * 
 * Internal imports
 * 
 * */

import ThreadManager.ThreadManager_flagCarrier;
import ThreadManager.ThreadManager_generic;

/**
 * 
 * Java imports
 * 
 * */

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Arrays;

import ErrorManager.ErrorManager_generic;
import FileManager.FileManager;

/**
 * 
 * Java imports
 * 
 * */



/**
 * Created by LeNo 6.9.2017..
 * 
 * 
 * Purpose of class:
 * 			-class that handles requests from mobile
 * 
 * */




public class ClientApi {
	
	private BufferedInputStream inptStrm;
	private BufferedOutputStream outStrm;
	private final ThreadManager_generic thdMng;
	private final ThreadManager_flagCarrier flgCarr;
	private final ErrorManager_generic errMng;
	
	
	private final FileManager fileMng;
	private final ByteManager byMng;
	
	private final String ipAddress;
	private final String session;
	
	public ClientApi(String ipAddr,
					 String sess,
					 BufferedInputStream inStream,
					 BufferedOutputStream outStream,
					 ThreadManager_generic thdM,
					 ErrorManager_generic errM)
	{
		
		ipAddress=ipAddr;
		session=sess;
		
		inptStrm=inStream;
		outStrm=outStream;
		thdMng=thdM;
		flgCarr=new ThreadManager_flagCarrier();
		errMng=errM;
		
		fileMng=new FileManager();
		byMng=new ByteManager(flgCarr);
	}
	
	
	public boolean checkObject(String ipAddr,String sess) 
	{
		return (ipAddr.equals(ipAddress) && sess.equals(session))? true:false;
	}
	
	public void reconnect(BufferedInputStream inStream,BufferedOutputStream outStream) 
	{
		inptStrm=inStream;
		outStrm=outStream;
		requestListen();
		
	}
	
	
	
	
	/**
	 * Requests handler functions
	 * 
	 * */
	
	public void requestListen() 
	{
		try 
		{
			System.out.println("Listening");	
			long a=System.currentTimeMillis();
			while(true) 
			{
				if (byMng.recvRequest(inptStrm,outStrm)) 
				{
					identRequest(byMng.getRequest());
				}
				
				if (System.currentTimeMillis()-a>5000) 
				{
					outStrm.close();
					System.out.println("shuttingdown");
					break;
				}
			}
			
		}
		catch(IOException e) 
		{
			errMng.handler(e);
		}
	}			
	

	
	
	
	
	private void identRequest(String request) throws IOException
	{
		byMng.cleanRequest();
		System.out.println("request\t"+request);

		switch(request)
		{
		case "ROOT*":rootRequest();break;
		case "CLOSE*":closeRequest();break;
		
		case "SELECTALL*":break;
		case "REMOVEALL*":break;
		
		case "DOWNLOAD*":break;
		case "UPLOAD*":break;
		
		case "OPENSTATUS_NSUCCESS*":fileMng.closeFolder();break;
		case "CLOSESTATUS_NSUCCESS*":fileMng.openFolder(fileMng.closeStatus);break;
		
		case "EXIT*":throw new IOException("Exit");
		
		default:
			{
				if (request.matches("\\bOPEN\\d+[*]"))openRequest(Integer.parseInt(request.substring(4,request.indexOf("*"))));
			}
		}
	}
	
	
	
	
	/**
	 * 
	 * "REQUEST HANDLER" FUNCTIONS
	 * 
	 * 
	 * */
	
	private void rootRequest()throws IOException 
	{
		send(fileMng.strBuffer_string().getBytes());
	}
	
	
	private void openRequest(int position) throws IOException
	{
		fileMng.openFolder(fileMng.files.get(position));

		send(fileMng.strBuffer_string().getBytes());
	}
	
	
	private void closeRequest() throws IOException
	{
		fileMng.closeFolder();
		
		send(fileMng.strBuffer_string().getBytes());
	}
		
	
	/**
	 * General use methods
	 * 
	 * */
	
	

	
	private void send(byte[] bytes)throws IOException 
	{
		outStrm.write(bytes);
		outStrm.flush();
		System.out.println("flushed");
	}
	

}
