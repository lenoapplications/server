package Enum;


/**
 * 
 * Created by LeNo on 6.9.2017..
 * 
 * 
 * Purpose of enum:
 * 
 * 			used for telling application is there some communication between server currently going on
 * 			
 * 
 * 			Example:
 * 				
 * */


public enum ServerStatus {
	
	/**
	 * 
	 * SERVER ->status on connection:
	 * 			
	 * 			S_CONNECTION_ACTIVE -> mobile connected to PC
	 * 			S_CONNECTION_NAACTIVE mobile not connect to PC
	 * 			
	 * 
	 * */
	
	
	S_CONNECTION_ACTIVE,
	S_CONNECTION_NACTIVE,

}
