package FileManager;

import java.util.HashSet;

/**
 * Created by LeNo 14.9.2017..
 * 
 * 
 * Purpose of class:
 * 			this will be "container" for files that have been choosed for upload to android
 * 
 * 
 * 			Description:
 * 				-> function "onClickFile"
 * 				   adds String absolute path to "set", but if absolutePath already in "set" then he will remove it from "set".
 * 					
 * 				example:
 * 					if user clicks on file,then file is choosed for downloading it to android
 * 					if user clicks again on the same file then file will be removed and wont download to android
 * 
 * 
 * */

public class FileCart extends HashSet<String> {
	

	
	public void onClickFile(String absPath) 
	{
		if(!add(absPath))this.remove(absPath);
	}
	
	@Override
	public boolean remove(Object o) {
		// TODO Auto-generated method stub
		
		for (String str:this) 
		{
			if (str.equals((String)o)) 
			{
				super.remove(str);
				return true;
			}
		}
		
		return false;
	}
}
