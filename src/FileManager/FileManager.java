package FileManager;




/**
 * 
 * Java imports
 * 
 * */
import java.util.ArrayList;
import java.util.Arrays;
import java.io.File;

/**
 * Created by LeNo 14.9.2017..
 * 
 * 
 * Purpose of class:
 * 			-> this class is used as "navigation system" through files.
 * 			   it will have ArrayList object that will contain current files,something like pointer
 * 			   to position of where are we currently in pc "storage".
 * 
 * 
 * 			Description:
 * 				
 * 				openFolder function:
 * 								->we clear current files in list
 * 								->strBuffer clear (because we are not anymore in root folder,we need to add new files to string)
 * 								->list String[] and add new Data object to "navigation system"|ArrayList object
 * 				closeFolder fuction:
 * 								-> same as openFolder except we are going backwards.
 * 								->clear current files and strBuffer
 * 								-> difference is we need root folder of current root
 * 											example(
 * 													
 * 													C:\
 * 														Program Files -> current "root" in "FileManager" object
 * 
 * 																ShareApp	->current folder navigation is pointing
 * 																		app.exe ->
 * 																					-> we add those two objects in Array and into strBuffer
 * 																		app.txt ->
 * 													When closing folder "Sharepp" we need to get the root of current root(ProgramFiles)
 * 													In this example, root of current root is "C:\" and then we "listfiles" of "C:\"
 * 	
 * 													)
 * 			 	addFiles:
 * 					-> add new Data object to ArrayList
 * 					-> add String absolute Path to strBuffer;
 * 			
 * 
 * */

public class FileManager {
	
	
	public ArrayList<Data> files;
	
	public Data closeStatus;
	
	private Data root;
		
	private final StringBuffer strBuffer;
	
	public FileManager() 
	{
		files=new ArrayList<>();
		
		strBuffer=new StringBuffer();

		roots_init();
	}
	
	
	
	private void roots_init() 
	{
		File[] roots=File.listRoots();
		
		root=null;
		
		Arrays.stream(roots).forEach(root->addFile(root, null));
		
		strBuffer.end();
			
	}
	
	
	
	/**
	 * 
	 * OPEN/CLOSE FOLDER SECTION
	 * 
	 * */
	
	
	public boolean openFolder(Data fldr) 
	{
		
		root=fldr;
		
		files.clear();
		
		strBuffer.clear();
		
		Arrays.stream(fldr.listFiles()).forEach(absPath->addFile(absPath, root));
		
		strBuffer.end();
		
		return (files.size()>0)?true:false;
	}
	
	
	
	public boolean closeFolder() 
	{
		try 
		{
			closeStatus=root;
			
			files.clear();
						
			strBuffer.clear();
			
			root=root.getRoot();
			
			Arrays.stream(root.listFiles()).forEach(absPath->addFile(absPath, root));
			
			strBuffer.end();
		
			return true;
		}
		catch(NullPointerException e) 
		{
		
			roots_init();
			return false;
		}
	}
		
	public String strBuffer_string() 
	{
		return strBuffer.toString();
	}
		
	
	/**--------------------------------------------------------------------------------------------------------**/
	
	
	
	private void addFile(File fl,Data rt) 
	{
		Data newData=new Data(fl.getAbsolutePath(), rt);
		
		files.add(newData);

		strBuffer.appendFile(newData);
		
	}
	
	private void addFile(String absPath,Data rt) 
	{
		Data newData=new Data(absPath,rt);

		files.add(newData);
		
		strBuffer.appendFile(newData);
	}
	
	
	
	
	/**
	 * 
	 * DEBUGG
	 * 
	 * */
	
	
	public void printCurrent() {
		
		for (Data a:files) {
			System.out.println(a.getName());
		}
	}
	
	public String getStrBuffer() {
		
		return strBuffer.toString();
	}
	

}
