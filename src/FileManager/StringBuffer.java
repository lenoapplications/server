package FileManager;

import java.util.Arrays;

import Enum.FileType;

/**
 * 
 * Java imports
 * 
 * */



/**
 * Created by LeNo 14.9.2017..
 * 
 * 
 * Purpose of class:
 * 			String that contains all files of current folders that we send to android
 * 
 * 
 * 			Description:
 * 					appendFile
 * 						-> function calls "parseData" function where file is being identified and 
 * 						   adding appropriate char next to file name.
 * 						Example:
 * 							if file is ROOT (C:\) then char "|" is added to file name (|C*) and end char '*'
 * 							Note:
 * 								if user choose file (clicks on item on android), char ':' is added to file name (">App.exe:'*')
 * 
 * */

public class StringBuffer{
	
	private StringBuilder builder=new StringBuilder();
	private final FileCart cart=new FileCart();
	
	
	public void appendFile(Data file) 
	{
		builder.append(parseData(file));
	}
	
	public void clear() 
	{
		builder.setLength(0);
	}
	
	public void end() 
	{
		builder.append("*");
	}
	
	
	public void itemClicked(String absPath)
	{
		cart.onClickFile(absPath);
	}
	
	
	private String parseData(Data file) 
	{
		String stringFile=((file.getType()==FileType.ROOT)? "|" : (file.getType()==FileType.DIRECTORY) ? "<" :">").concat(file.getName());
		
		for (String chsFile:cart) 
		{
			if (chsFile.equals(file.getAbsPath())) return stringFile.concat(":");
		}
		return stringFile;
	}
	
	
	
	

	
	
	
	@Override
	public String toString() 
	{	
		return builder.toString();
	}
}
