package FileManager;


/**
 * 
 * Internal imports
 * 
 * */
import Enum.FileType;


/**
 * 
 * Java imports
 * */
import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.regex.Pattern;

/**
 * Created by LeNo 14.9.2017..
 * 
 * 
 * Purpose of class:
 * 			
 * 
 * */

public class Data {
	
	
	private final String absPath;
	private final Data homeFolder;
	private final FileType type;
	
	
	
	public Data(String absPth,Data hmFld ) 
	{
		
		absPath=absPth;
		homeFolder=hmFld;
		type=checkType();
	}
	
	
	private FileType checkType() 
	{
		if (homeFolder==null)return FileType.ROOT;
		
		else if (isDir())return FileType.DIRECTORY;
		
		else return FileType.FILE;
	}
	
	
	
	/**
	 * 
	 * Public functions
	 * 		extracting informations about file
	 * 
	 * */
	
	public boolean isDir() 
	{
		return new File(absPath).isDirectory();
	}
	
	
	public FileType getType() 
	{
		return type;
	}
	
	
	public Data getRoot() 
	{
		return homeFolder;
	}
	
	
	public String getAbsPath() 
	{
		return absPath;
	}
	
	
	public String getName() 
	{
		for (int i=absPath.length()-1;i>-1;i--) 
		{
			if (absPath.charAt(i)=='\\') 
			{
				return (type==FileType.ROOT)?absPath.substring(0,i-1):absPath.substring(i+1);
			}
		}	
		return null;
	}
	
		
	public String[] listFiles() 
	{
		ArrayList<String> files=new ArrayList<>();
		
		if (type==FileType.DIRECTORY || type==FileType.ROOT) 
		{
			for (File file:new File(absPath).listFiles(new Filter())) 
			{
				files.add(file.getAbsolutePath());
			}
			return files.toArray(new String[files.size()]);
		}
		
		return null;
	}
	
	
	
	
	/**
	 * 
	 * Inner class Filter
	 * 
	 * */
	
	
	class Filter implements FileFilter{
		
		private final Pattern pattern=Pattern.compile(".+\\.(mp3|mp4|jpg|png|jpeg|JPEG|JPG|txt|pdf|)");
		
		@Override
		public boolean accept(File pathname) {
	
			if (pattern.matcher(pathname.getName()).matches())return true;
			
			else if (pathname.isDirectory())return true;
			
			return false;
		}	
	}

}
