package ThreadManager;


/**
 * Threads internal imports
 * 
 * */

import ErrorManager.ErrorManager_generic;

import java.io.IOException;

/**
 * Java imports
 * 
 * */

import java.lang.reflect.Method;






/**
 * 
 * Created by LeNo on 6.9.2017..
 * 
 * 
 * 
 * 
 * Purpose of class:
 * 			-class for function that will manage all processes that needs to be run on another thread
 * 
 * */

public class ThreadManager_generic {
	
	private final int T_SERVER=0;
	
    private final ThreadManager_flagCarrier flgCarr;
    
    
    public ThreadManager_generic(){

        flgCarr=new ThreadManager_flagCarrier();
    }


    public <A,B,C> void thread_Init(final Object methodObj,
                                         final String methodName,
                                         final ErrorManager_generic errMng,
                                         final A objA,
                                         final B objB,
                                         final C objC,
                                         Class<A> a,
                                         Class<B>b,
                                         Class<C>c,
                                         int threadID)
    {
    	
    	{  	
        	try 
        	{
        		if (checkFreeThreads(threadID))
        		{
        			final Method mth=methodObj.getClass().getMethod(methodName,ErrorManager_generic.class,a,b,c);
    				startThread(methodObj, mth,errMng,objA,objB,objC);
        		}
        	}
        	catch(NoSuchMethodException e)
        	{
        		e.printStackTrace();
        	}
        }

    }


    public <A,B> void thread_Init(final Object methodObj,
                                         final String methodName,
                                         final ErrorManager_generic errMng,
                                         final A objA,
                                         final B objB,
                                         Class<A> a,
                                         Class<B>b,
                                         int threadID)
   {
    	
    	try 
    	{
    		if (checkFreeThreads(threadID))
    		{
    			final Method mth=methodObj.getClass().getMethod(methodName,ErrorManager_generic.class,a,b);
				startThread(methodObj, mth,errMng,objA,objB);
    		}
    	}
    	catch(NoSuchMethodException e)
    	{
    		e.printStackTrace();
    	}
    }
    
    

    public <A> void thread_Init(final Object methodObj,
                                final String methodName,
                                final ErrorManager_generic errMng,
                                final A objA,
                                Class<A> a,
                                int threadID)
    {
    	
    	try 
    	{
    		if (checkFreeThreads(threadID))
    		{
    			final Method mth=methodObj.getClass().getMethod(methodName,ErrorManager_generic.class,a);
				startThread(methodObj, mth,errMng,objA);
    		}
    	}
    	catch(NoSuchMethodException e)
    	{	
    		e.printStackTrace();
    	}
    }
    
    public void thread_Init_noargs(final Object methodObj,final String methodName) 
    {
    	try 
    	{
    		final Method mth=methodObj.getClass().getMethod(methodName);
    		startThread(methodObj, mth);
    	}
    	catch(NoSuchMethodException e) 
    	{
    		e.printStackTrace();
    	}
    }

    
    
    private boolean checkFreeThreads(int threadID) {
    	
    	
    	
    	switch(threadID)
    	{
    	
    		case T_SERVER:
    		{
    			return true;
    		}
    	}
    	
    	return false;
    }

    
    


    private void startThread(final Object methodObj,
    					     final Method mth,
    					     final Object...args){

        new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                try
                {
                    mth.invoke(methodObj,args);
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                }
            }
        }).start();
    }
}
