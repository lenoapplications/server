package ThreadManager;



/**
*
* Internal imports
*
* */

import Enum.ThreadManager_enums;
import Enum.ServerStatus;

/**
*
* Java imports
*
* */








/**
 * Created by LeNo on 5.9.2017..
 *
 *
 * Purpose of class:
 *          This class will be used like "Semaphore", it will hold enum flags of "ThreadManagerEnum",
 *          that why "ThreadManager_generic" can communicate with threads.
 *
 *
 *			
 *			Section of class:
 *					First section:
 *								-> first section of class is for connection status between mobile and PC thread.
 *								It consists of enum and methods for changing status and getting current
 *								status on thread
 *
 *
 */


public class ThreadManager_flagCarrier {
	
	private ThreadManager_enums T_serverConn_flag;
	
	private ServerStatus S_clientConnection_flag;
	
	public ThreadManager_flagCarrier() 
	{	
		S_clientConnection_flag=ServerStatus.S_CONNECTION_ACTIVE;
	}
	
	
	
	
	
	public boolean S_serverConn_isActive() {return S_clientConnection_flag==ServerStatus.S_CONNECTION_ACTIVE;}
	
	public void S_serverConn_Activate() {S_clientConnection_flag=ServerStatus.S_CONNECTION_ACTIVE;};
	
	public void S_serverConn_Deactivate() {S_clientConnection_flag=ServerStatus.S_CONNECTION_NACTIVE;};
	
	
	
	
	

}
